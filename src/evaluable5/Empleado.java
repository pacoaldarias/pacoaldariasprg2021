/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluable5;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 3 feb. 2021 20:12:16
 */
public abstract class Empleado extends Persona {

   private String IdEmpleado;

   // Constructores
   public Empleado() {
      super();
      IdEmpleado = "No existe";
   }

   public Empleado(String nombre, String apellidos, int edad, String id) {
      super(nombre, apellidos, edad);
      IdEmpleado = id;
   }

   // Métodos
   public void setIdEmpleado(String IdProfesor) {
      this.IdEmpleado = IdProfesor;
   }

   public String getIdEmpleado() {
      return IdEmpleado;
   }

   public void mostrarDatos() {

      System.out.println("Datos Empleado. Empleado de nombre: " + getNombre() + " "
              + getApellidos() + " con Id de Empleado: " + getIdEmpleado());
   }

   public String toString() {
      return super.toString().concat(" IdEmpleado: ").concat(IdEmpleado);
   }

   abstract public float importeNomina();  // Método abstracto

}
