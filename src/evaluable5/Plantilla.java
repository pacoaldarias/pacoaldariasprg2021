/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluable5;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 3 feb. 2021 20:26:08
 */
public class Plantilla {

   private ArrayList<Empleado> plantilla; //Campo de la clase

   public Plantilla() {
      plantilla = new ArrayList<Empleado>();
   } //Constructor

   public void addEmpleado(Empleado empleado) {
      plantilla.add(empleado);
   } //Método

   public void imprimirPlantilla() {  //Método

      String tmpStr1 = ""; //String temporal que usamos como auxiliar

      System.out.println("Se procede a mostrar los datos de los empleados existentes en la plantilla");

      int cont = 1;

      for (Empleado tmp : plantilla) {

         System.out.print(cont + ". ");
         System.out.println(tmp.toString());

         if (tmp instanceof Contratado) {
            tmpStr1 = "Contratado";
         } else {
            tmpStr1 = "Fijo";
         }
         System.out.println("Tipo de este empleado:" + tmpStr1 + " Nómina de este empleado: " + (tmp.importeNomina()));
         cont++;
      }

   } //Cierre método imprimirPlantilla

   public float importeTotalNominaEmpleados() {

      float importeTotal = 0f; //Variable temporal que usamos como auxiliar

      Iterator<Empleado> it = plantilla.iterator();

      while (it.hasNext()) {
         importeTotal = importeTotal + it.next().importeNomina();
      }

      return importeTotal;

   } //Cierre del método importeTotalNominaEmpleados

}
