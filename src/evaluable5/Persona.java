/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluable5;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 3 feb. 2021 20:07:44
 */
public class Persona {

   private String nombre;
   private String apellidos;
   private int edad;

   public Persona() {
      nombre = "";
      apellidos = "";
      edad = 0;
   }

   public Persona(String nombre, String apellidos, int edad) {
      this.nombre = nombre;
      this.apellidos = apellidos;
      this.edad = edad;
   }

   public String getNombre() {
      return nombre;
   }

   public String getApellidos() {
      return apellidos;
   }

   public int getEdad() {
      return edad;
   }

   public String toString() {
      Integer datoEdad = edad;
      return "Nombre: ".concat(nombre).concat(" Apellidos: ").concat(apellidos).concat(" Edad: ").concat(datoEdad.toString());
   }

}
