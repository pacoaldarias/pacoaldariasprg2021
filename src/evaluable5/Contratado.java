/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluable5;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 3 feb. 2021 20:18:23
 */
public class Contratado extends Empleado {

   private int diascontrato = 0;

// Constructores
   public Contratado(int diascontrato) {

      super();
      this.diascontrato = diascontrato;
   }

   public Contratado(String nombre, String apellidos, int edad, String id, int diascontrato) {

      super(nombre, apellidos, edad, id);

      this.diascontrato = diascontrato;
   }

   public int getFechaComienzoInterinidad() {
      return diascontrato;
   } //Método

   public String toString() { // Sobreescritura del método
      Integer datoEdad = diascontrato;
      return super.toString().concat(" Dias contrato: ").concat(datoEdad.toString());
   }

   public float importeNomina() {
      return 1000;
   } //Método abstracto sobreescrito en esta clase

}
