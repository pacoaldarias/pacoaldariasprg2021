/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluable5;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 3 feb. 2021 20:07:16
 *
 */
public class Main {

   /**
    * @param args the command line arguments
    */
   public static void main(String[] args) {
      // TODO code application logic here

      Contratado c1 = new Contratado("Pepe", "Reina", 45, "1258978-J", 12);
      Contratado c2 = new Contratado("Pedro", "Trujillo", 87, "5666899-L", 30);
      Contratado c3 = new Contratado("Rosa", "Arias", 76, "89788993-X", 50);

      Fijo f1 = new Fijo("Paco", "Aldarias", 23, "78899553-H");
      Fijo f2 = new Fijo("Juan", "Espinosa", 49, "8878994-L");
      Fijo f3 = new Fijo("Luis", "Davila", 49, "85556661-F");

      Plantilla plantilla = new Plantilla();
      plantilla.addEmpleado(c1);
      plantilla.addEmpleado(c2);
      plantilla.addEmpleado(c3);
      plantilla.addEmpleado(f1);
      plantilla.addEmpleado(f2);
      plantilla.addEmpleado(f3);
      plantilla.imprimirPlantilla();

      System.out.println("El importe de las nóminas de los empleados que consta en la plantilla es "
              + plantilla.importeTotalNominaEmpleados() + " euros");

   }

}
