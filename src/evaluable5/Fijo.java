/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluable5;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * 3 feb. 2021 20:16:27
 */
public class Fijo extends Empleado {

   public Fijo(String nombre, String apellidos, int edad, String id) {
      super(nombre, apellidos, edad, id);
   }

   public float importeNomina() {
      return 1500;
   }  //Método abstracto sobreescrito en esta clase

}
